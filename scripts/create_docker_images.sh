#!/bin/bash

PWD="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

function createDockerImage() {
	cd $PWD/../docker/$imageName

	sudo docker build --no-cache -t $imageName:$version -t $imageName:latest ./

	cd -
}

imageName=qt-compiler-base
version=1.0.0

createDockerImage

imageName=qt-compiler-boost-ldap
version=1.0.0

createDockerImage