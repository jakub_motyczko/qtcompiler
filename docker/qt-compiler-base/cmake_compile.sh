#!/bin/bash

qtPath=$1
projectPath=$2
buildDir=$3

mkdir $buildDir

cd $buildDir
CMAKE_PREFIX_PATH=$qtPath /usr/bin/cmake -S $projectPath -B $buildDir

make -j`nproc --ignore=1`

