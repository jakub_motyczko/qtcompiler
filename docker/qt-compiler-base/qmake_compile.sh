#!/bin/bash

qmakePath=$1
projectPath=$2
buildDir=$3

mkdir $buildDir

cd $buildDir
$qmakePath $projectPath

make -j`nproc --ignore=1`
