# QtCompiler

It's an example of using docker for compiling Qt based projects utilizing QMake and CMake

## Structure

### Docker images

This example uses 2 docker images:
- qt-compiler-base - base docker image, only with Qt 
- qt-compiler-boost-ldap - example docker image based on `qt-compiler-base` with additional dependencies: boost and ldap

### Docker services

`docker-compose.yml` contains docker compose service definitions:
- build-cmake - service for compiling cmake based applications
- run-cmake - service for getting into docker image. e.g. to install new dependencies and test if they'll work
- build-qmake - service for compiling qmake based applications

Docker services have a `command` which uses Qt installed in `/mnt/host/opt/Qt5.11.1/5.11.1/gcc_64/`. Please change it in case you're using different Qt version.

### Projects

This example has 2 almost empty CMake and QMake projects to test if docker images have been properly installed.

- projects/CMakeProject
- projects/QMakeProject

## Building docker images

To build docker images please use `scripts/create_docker_images.sh`. It uses `docker build` command for dirs mentioned in `Docker images`

## Compiling examples

Example usage is:

- `cd docker`
- `ls -l docker-compose.yml` - to make sure you're in a right dir
- CMake project:
`PROJECT_PATH=/home/USER/workspace/docker/QtCompiler/projects/CMakeProject  docker-compose run build-cmake`
- QMake project:
`PROJECT_PATH=/home/USER/workspace/docker/QtCompiler/projects/QMakeProject  docker-compose run build-qmake`

## Adding new docker image

For example you have a cmake project that uses Qt and libusb.

- `cd docker`
- `cp -r qt-compiler-boost-ldap qt-compiler-libusb`
- `sed -i -e 's/libboost-all-dev libldap2-dev/libusb-dev/g' qt-compiler-libusb/Dockerfile`
- `sed -i -e 's/qt-compiler-boost-ldap/qt-compiler-libusb/g' docker-compose.yml`
- `cd qt-compiler-libusb`
- `docker build --no-cache -t qt-compiler-libusb:latest ./`

You can now build your project using updated image:

`PROJECT_PATH=/home/USER/workspace/docker/QtCompiler/projects/CMakeProject  docker-compose run build-cmake`
